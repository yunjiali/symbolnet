/**
 * Schema for lexical entries
 * v0.4.0: add votes for sameas, sentences and wordnet
 * v0.3.1: add staticstical static methods
 * v0.3.0: add static methods for calculating points, upvotes, downvotes, image creation (credits), lexicon creation (credits)
 * v0.2.2: add default language for images
 * v0.2.1: comment->comments, String->String Array
 * v0.2.0: add Index for LexiconEntry.label
 * v0.1.4: add ranking attribute for each image, audio and video
 * v0.1.3: add comment for vote
 * v0.1.2: add a static method to randomly select a lexicon entry
 * v0.1.1: add prov fields for wordnet and sameas
 * v0.1.0: add static methods
 * v0.0.2: add sentences objects (Schema)
 * v0.0.1: init the file (Schema)
 */
var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , _=require('underscore');
  
var LexicalEntrySchema = new Schema({
	label:{type:String,trim:true, index:true},
	label_id:Number,
	creator:String,
	created_at: { type: Date, default: Date.now },
	description: String,
	sentences:[{
		text:{type:String, trim:true},
		created_at:{ type: Date, default: Date.now },
		creator: String,
		votes:[{
			voter: String,
			val: {type:Number, max:1, min:-1},
			voted_at: { type: Date, default: Date.now },
			comments: [{type:String, trim:true}]
		}]
	}],
	pos: String,
	language: String,
	wordnet:[{
		synset_offset: String,
		version: String,
		language: String,
		created_at: { type: Date, default: Date.now },
		creator: String,
		votes:[{
			voter: String,
			val: {type:Number, max:1, min:-1},
			voted_at: { type: Date, default: Date.now },
			comments: [{type:String, trim:true}]
		}]
	}],
	sameas:[{
		uri: {type:String,trim:true},
		created_at: { type: Date, default: Date.now },
		creator:String,
		votes:[{
			voter: String,
			val: {type:Number, max:1, min:-1},
			voted_at: { type: Date, default: Date.now },
			comments: [{type:String, trim:true}]
		}]
	}],
	images:[{
		image_uri: {type:String,trim:true},
		description: String,
		language: {type:String,default:"en"},
		ranking: {type:Number, default:0},
		creator:String,
		created_at: { type: Date, default: Date.now },
		license: String,	
		votes:[{
			voter: String,
			val: {type:Number, max:1, min:-1},
			voted_at: { type: Date, default: Date.now },
			comments: [{type:String, trim:true}]
		}]
	}],
	audios:[{
		audio_uri: {type:String,trim:true},
		description: String,
		language: {type:String,default:"en"},
		ranking: {type:Number, default:0},
		creator:String,
		created_at: { type: Date, default: Date.now },
		license: String,	
		votes:[{
			voter: String,
			val: {type:Number, max:1, min:-1},
			voted_at: { type: Date, default: Date.now },
			comments: [{type:String, trim:true}]
		}]
	}],
	videos:[{
		video_uri: {type:String,trim:true},
		description: String,
		language: {type:String,default:"en"},
		ranking: {type:Number, default:0},
		creator:String,
		created_at: { type: Date, default: Date.now },
		license: String,	
		votes:[{
			voter: String,
			val: {type:Number, max:1, min:-1},
			voted_at: { type: Date, default: Date.now },
			comments: [{type:String, trim:true}]
		}]
	}],
});

LexicalEntrySchema.statics = {

	/**
   	* List only the image URIs contained in the corresponding lexical entries, regardless which lexicon the image belongs to
   	*	
   	* @param {Object} condition
   	* @param {Function} callback
   	* @api private
   	*/
	listImagesURIs: function(condition,callback){
		
		this.find(condition,
			function(err,entries){
				if(err)
				{
					return callback(err);
				}

				if(entries.length > 0)
				{
					var images = new Array();
					console.log("l:"+entries.length)
					for(var i=0;i<entries.length; i++)
					{
						for(var j=0;j<entries[i].images.length;j++)
						{
							//console.log("uri:"+entries[i].images[j].image_uri);
							var image_uri = entries[i].images[j].image_uri;
							var oimage=_.findWhere(images,{uri:image_uri});
							if(oimage === undefined)
								images.push({entry_ids:[entries[i]._id],uri:image_uri});
							else
							{
								if(!_.contains(oimage.entry_ids,entries[i]._id))
								{
									oimage.entry_ids.push(entries[i]._id);
								}
							}

						}
					}

					if(images.length == 0)
						return callback(null, []);
					else
					{
						//console.log(_.uniq(images));
						return callback(null,_.uniq(images));
					}
				}
				else
				{
					return callback(null, []); //check stemmable words

				}
			}
		);
	},

	/**
   	* Randomly select an entry. If creator is not null, select an entry that the user hasn't commened on
   	*	
   	* @param {String} creator
   	* @param {Function} callback
   	* @api private
   	*/
	getRandomEntry: function(creator,callback){
		this.count(function(err, count) {
		    if (err) return callback(err);
		    var rand = Math.floor(Math.random() * count);
		    this.findOne({"images.creator":{$ne:creator}}).skip(rand).exec(callback);
		}.bind(this));
	},

	//############## statistical methods ###############
	countLexicalEntryTotal:function(callback){
		this.count(function(err,count){
			if (err) {
				return callback(err);
			}
			return callback(null,count);
		});
	},
	countLexicalEntryWithoutImages:function(callback){
		this.where('images').size(0).count(function(err,count){
			if (err) {
				return callback(err);
			}
			return callback(null,count);
		});
	},
	countImagesTotal:function(callback){
		this.aggregate([
			{$project:{images:1}},
			{$unwind:"$images"},
			{$group:{
				_id:"images",
				totalImages:{$sum:1}
			}}
		],function(error,imagesCount){
			callback(null,imagesCount[0].totalImages);
		});
	},
	countVotedImagesTotal:function(callback){
		this.aggregate([
			{$project:{images:1}},
			{$unwind:"$images"},
			{$unwind:"$images.votes"},
			{$group:{
				_id:"$images._id",
				totalVotes:{$sum:1}
			}}
		],function(error,images){
			//console.log("2:"+images.length);
			callback(null,images.length);
		});
		 /*this.distinct("images._id",function(err,result){
		 	if (err) return callback(err,null);
		 	return callback(null,result.length);
		 });*/
	},
	countVotesTotal:function(callback){
		this.distinct("images.votes._id",function(err,result){
		 	if (err) return callback(err,null);
		 	return callback(null,result.length);
		});
	},
	countSentencesTotal:function(callback){
		this.distinct("sentences._id",function(err,result){
		 	if (err) return callback(err,null);
		 	return callback(null,result.length);
		});
	},
	countSameAsTotal:function(callback){
		this.distinct("sameas._id",function(err,result){
		 	if (err) return callback(err,null);
		 	return callback(null,result.length);
		});
	},
	getLexiconCreationCount:function(creator,callback){

	},
	getSentenceCreationCount:function(creator,callback){

	},
	getSameAsCreationCount:function(creator,callback){

	}
}

mongoose.model('LexicalEntry', LexicalEntrySchema);