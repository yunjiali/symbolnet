/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , crypto = require('crypto')
  , _ = require('underscore')
  , authTypes = ['github', 'twitter', 'facebook', 'google']

/**
 * User Schema
 */

var UserSchema = new Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  username: { type: String, default: '' },
  provider: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  authToken: { type: String, default: '' },
  facebook: {},
  twitter: {},
  github: {},
  google: {},
  credits:{
    upvotes:{
        count:{type:Number,default:0,min:0},
        credits:{type:Number,default:0,min:0}
    },
    downvotes:{
      count:{type:Number,default:0,min:0},
      credits:{type:Number,default:0,min:0}
    },
    lexicons:{
      count:{type:Number,default:0,min:0},
      credits:{type:Number,default:0,min:0}
    },
    images:{
      count:{type:Number,default:0,min:0},
      credits:{type:Number,default:0,min:0}
    },
    sentences:{
      count:{type:Number,default:0,min:0},
      credits:{type:Number,default:0,min:0}
    },
    sameas:{
      count:{type:Number,default:0,min:0},
      credits:{type:Number,default:0,min:0}
    }
  }
})

/**
 * Virtuals
 */

UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function() { return this._password })

/**
 * Validations
 */

var validatePresenceOf = function (value) {
  return value && value.length
}

// the below 4 validations only apply if you are signing up traditionally

UserSchema.path('name').validate(function (name) {
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) return true
  return name.length
}, 'Name cannot be blank')

UserSchema.path('email').validate(function (email) {
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) return true
  return email.length
}, 'Email cannot be blank')

UserSchema.path('email').validate(function (email, fn) {
  var User = mongoose.model('User')
  
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) fn(true)

  // Check only when it is a new user or when email field is modified
  if (this.isNew || this.isModified('email')) {
    User.find({ email: email }).exec(function (err, users) {
      fn(!err && users.length === 0)
    })
  } else fn(true)
}, 'Email already exists')

UserSchema.path('username').validate(function (username) {
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) return true
  return username.length
}, 'Username cannot be blank')

UserSchema.path('hashed_password').validate(function (hashed_password) {
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) return true
  return hashed_password.length
}, 'Password cannot be blank')


/**
 * Pre-save hook
 */

UserSchema.pre('save', function(next) {
  if (!this.isNew) return next()

  if (!validatePresenceOf(this.password)
    && authTypes.indexOf(this.provider) === -1)
    next(new Error('Invalid password'))
  else
    next()
})

/**
 * Methods
 */

UserSchema.methods = {

  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */

  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */

  makeSalt: function () {
    return Math.round((new Date().valueOf() * Math.random())) + ''
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */

  encryptPassword: function (password) {
    if (!password) return ''
    var encrypred
    try {
      encrypred = crypto.createHmac('sha1', this.salt).update(password).digest('hex')
      return encrypred
    } catch (err) {
      return ''
    }
  },

  /**
   * Get user's pictuer
   */
  getPicture: function(size){
    if(this.provider=== "google")
    {
      if(!this.google.picture)
        return undefined;
      return this.google.picture+"?sz="+size;
    }
    else if(this.provider=== "twitter")
    {
      if(!this.twitter.profile_image_url)
        return undefined;
      if(size <=24)
        return this.twitter.profile_image_url.replace("normal","mini");
      else if(size>73)
        return this.twitter.profile_image_url.replace("normal","bigger");
    }
    else
      return undefined;
  },

  //############## Badging System related methods #################
  getTotalCredits:function(){
    return this.credits.upvotes.credits+this.credits.downvotes.credits+this.credits.lexicons.credits+
            this.credits.images.credits+this.credits.sentences.credits+this.credits.sameas.credits;
  },

  getBadge:function(){
    var credits = this.getTotalCredits();
    if(credits <10){
      return {name:"Newbie",permissions:[true,false,false,false,false]};
    }
    else if(credits>=10 && credits<30){
      return {name:"Freshman",permissions:[true,true,false,false,false]};
    }
    else if(credits>=30 && credits<299){
      return {name:"Bachelor",permissions:[true,true,true,false,false]};
    }
    else if(credits>=300 && credits<999){
      return {name:"Doctor",permissions:[true,true,true,true,false]};
    }
    else if(credits>=1000){
      return {name:"Professor",permissions:[true,true,true,true,true]};
    }
  },

  canUpvote:function(){
    return true; // currently, everyone can upvote as long as registed
  },
  canCreateLexicon:function(){
    return true;
  },
  
  canUploadImage:function(){
    var credits = this.getTotalCredits();
    return credits>=10?true:false; 
  },

  canDownvote:function(){
    var credits = this.getTotalCredits();
    return credits>=30?true:false; 
  },
  canAddSentence:function(){
    var credits = this.getTotalCredits();
    return credits>=30?true:false; 
  },
  canAddSameAs:function(){
    var credits = this.getTotalCredits();
    return credits>=30?true:false; 
  },

  canEditLabel:function(){
    var credits = this.getTotalCredits();
    return credits>=300?true:false; 
  },
  canEditDescription:function(){
    var credits = this.getTotalCredits();
    return credits>=300?true:false; 
  },

  canRemoveSentence:function(){
    var credits = this.getTotalCredits();
    return credits>=1000?true:false; 
  },
  canRemoveSameAs:function(){
    var credits = this.getTotalCredits();
    return credits>=1000?true:false; 
  },

  addUpvoteCredit:function(){
    this.credits.upvotes.count+=1;
    this.credits.upvotes.credits+=this.constructor.getUpvoteCredit();
    return;
  },
  addDownvoteCredit:function(){
    this.credits.downvotes.count+=1;
    this.credits.downvotes.credits+=this.constructor.getDownvoteCredit();
    return;
  },
  addNewLexiconCount:function(){
    this.credits.lexicons.count+=1;
    return;
  },
  addNewLexiconCredit:function(){
    this.credits.lexicons.credits+=this.constructor.getNewLexiconCredit();
    return;
  },
  addUploadImageCount:function(){
    this.credits.images.count+=1;
    return;
  },
  addUploadImageVoteupCredit:function(){
    this.credits.images.credits+=this.constructor.getUploadImageVoteupCredit();
    return;
  },
  addNewSentenceCredit:function(){
    this.credits.sentences.count+=1;
    this.credits.sentences.credits+=this.constructor.getNewSentenceCredit();
    return;
  },
  addNewSameAsCredit:function(){
    this.credits.sameas.count+=1;
    this.credits.sameas.credits+=this.constructor.getNewSameAsCredit();
    return;
  }

  //############## End of Badging System related methods #################
}

UserSchema.statics = {

  //############## Badging System related methods #################  
  getUpvoteCredit:function(){
    return 1;
  },
  getDownvoteCredit:function(){
    return 1;
  },
  getNewLexiconCredit:function(){
    return 2;
  },
  getUploadImageVoteupCredit:function(){
    return 5;
  },
  getNewSentenceCredit:function(){
    return 1;
  },
  getNewSameAsCredit:function(){
    return 1;
  }
  //############## End of Badging System related methods #################
}

mongoose.model('User', UserSchema)
