var mongoose = require('mongoose');
var _=require('underscore');
var S = require('string');
var LexicalEntry = mongoose.model('LexicalEntry');
var User = mongoose.model('User');
var cloudinary = require('cloudinary');
var utils = require('../../lib/utils');

exports.upload = function(req,res)
{
	var entryId = req.query.entry_id;
	if(entryId === undefined){
		return res.render('400',{
			error: "Cannot find entry id."
		});
	}

	var cloudinary_cors = "http://" + req.headers.host + "/cloudinary_cors.html";

	LexicalEntry.findById(entryId, function(err,entry){
		if(err || entry === undefined)
		{
			return res.render('500',{error: "Can't find the entry."});
		}

		res.render('images/upload',{
			entry:entry,
			title: "Upload new symbole for "+entry.label,
			cloudinary:cloudinary
		});
	});
}

exports.handleUpload = function(req,res){
	if(req.body.entry_id === undefined || 
		req.body.image_uri === undefined ||
			req.body.description === undefined ||
				req.body.license === undefined){
		return res.send('400', "Bad request. Parameters missing.")
	}

	var entryId = req.body.entry_id;
	var image_uri = decodeURIComponent(req.body.image_uri);
	var description = decodeURIComponent(req.body.description);
	var license = decodeURIComponent(req.body.description);

	if(S(image_uri).isEmpty() || S(description).isEmpty() || S(license).isEmpty()){
		return res.send('400', "Bad request. Parameters are invalid.")
	}

	var img = new Object();
	img.image_uri = image_uri;
	img.description = description;
	img.language = "en";
	img.creator = req.user.id;
	img.license = license;

	
	LexicalEntry.findByIdAndUpdate(entryId, {$push:{"images":img}},function(err,entry){
		if(err || entry === undefined)
		{
			return res.send('500',"Can't find the entry.");
		}

		var user = req.user;
		user.addUploadImageCount();
		user.save(function(err,user,numberAffected){
			if(err)
				console.log(err);
		});

		if(entry.images.length === 1){ //first image upload, award points to owner
			User.findByIdAndUpdate(entry.creator, {$inc:{"credits.lexicons.credits":User.getNewLexiconCredit()}}, function(err){
				if(err)
					console.log("Image upvote credits:"+err);
			});
		}

		res.send('200');
	});
}