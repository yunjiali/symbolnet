var mongoose = require('mongoose');
var pos = require('pos');
var natural = require('natural');
var _=require('underscore');
var async = require('async');
var LexicalEntry = mongoose.model('LexicalEntry');
var utils = require('../../lib/utils');

/**
* Get all images for the given pos and word
*	
* @param {String} pos
* @param {String} word
* @return 
* result:{
	original:{
		word: String
		images: [String]
	},
	stemmed:{
		word:String
		images:[String]
	}
}
*/
exports.symbolise = function(req,res){
	var pos = req.query.pos;
	var word = decodeURIComponent(req.query.word);

	if(!pos || !word)
	{
		res.statusCode = 400;
		return res.send("Invalid request.")
	}

	var wnpos = utils.getWordNetPOS(pos);

	async.waterfall([
			function(callback){
				var result = new Object();
				result.original = new Object();
				result.original.word = word;
				result.original.images = [];
				result.stemmed = new Object();
				result.stemmed.word = word;
				result.stemmed.images= [];

				LexicalEntry.listImagesURIs({"label":word, "pos":wnpos}, function(err,images){
					if(err)
					{
						callback(err);
					}

					if(images.length ===0)
					{
						callback(null, true, result); 
					}
					else
					{
						result.original.word=word;
						result.original.images = images;
						callback(null,false, result);
					}
				});
			},
			function(checkStem, result, callback){
				if(checkStem === false)
				{
					callback(null, result);
				}
				else // check stemmable words
				{
					var stemmer = natural.LancasterStemmer;
					stemmer.attach();
					var stemmedWord = word.stem();
					console.log("stemmed word:"+stemmedWord);
					if(stemmedWord !== word)
					{
						LexicalEntry.listImagesURIs({label:stemmedWord, pos:wnpos}, function(err,images){
							if(err)
							{
								callback(err);
							}

							if(images.length ===0)
							{
								callback(null, result); 
							}
							else
							{
								result.stemmed.word=stemmedWord;
								result.stemmed.images = images;
								callback(null, result);
							}
						});
					}
				}
			}
		],
		function(err,result){
			if(err) 
			{
				res.statusCode = 500;
				return res.send("Interal Error." +err);
			}

			return res.json(result);
		}
	);
	
	
}