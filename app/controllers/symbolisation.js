var mongoose = require('mongoose');
var pos = require('pos');
var natural = require('natural');
var _=require('underscore');
var S = require('string');
var SentenceTokenizer = require('sentence-tokenizer');
var st = new SentenceTokenizer('Chunk');
var LexicalEntry = mongoose.model('LexicalEntry');
var utils = require('../../lib/utils');

exports.index = function(req,res){
	res.render('symbolisation/index');
}

exports.show = function(req,res){
	var text = req.body.text;
	if(text == null || S(text).trim().length == 0)
	{
		return res.render('symbolisation/index', {
	        errors: utils.errors("Text is empty.")
	    });
	}
	st.setEntry(text);
	var sentences = st.getSentences();

	var sentenceArray = new Array();
	for(var i=0; i< sentences.length; i++)
	{
		var sentence = sentences[i];
		var sObj = new Object();
		sObj.text = sentence;
		sObj.index = i;
		var words = new pos.Lexer().lex(sentence);
		var taggedWords = new pos.Tagger().tag(words);
		var taggedWordsArray = new Array();
		//TODO: Add Noun Phrase detection later
		for(item in taggedWords){
			var taggedWord = new Object();
			taggedWord.word = taggedWords[item][0];
			taggedWord.pos = taggedWords[item][1];
			taggedWordsArray.push(taggedWord);
		}
		sObj.words = taggedWordsArray;
		sentenceArray.push(sObj);
	}
	
	res.render('symbolisation/show',{
		sentences: sentenceArray
	});
}