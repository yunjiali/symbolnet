var mongoose = require('mongoose');
var pos = require('pos');
var natural = require('natural');
var async = require('async');
var _=require('underscore');
var S = require('string');
var SentenceTokenizer = require('sentence-tokenizer');
var st = new SentenceTokenizer('Chunk');
var numeral = require('numeral');
var LexicalEntry = mongoose.model('LexicalEntry');
var User = mongoose.model('User');
var utils = require('../../lib/utils');

exports.list = function(req,res){
	var q = req.body.q;
	var pos = req.body.pos;
	if(pos === undefined)
		pos = "all";
	if(q===undefined) //list the latest 10 images
	{
		LexicalEntry.find({}).sort({created_at:-1}).limit(10).exec(
			function(err,entries){
				if(err)
				{
					return res.render('500');
				}

				res.render('lexicons/list',{
					q:q,
					entries: addCanVoteToEntries(entries,req.user),
					vote:false,
					title: "List latest lexicons"
				});
			}
		);
	}
	else
	{
		var conditions = {label:q};
		if(pos === "unspecified")
		{
			conditions.pos= {$ne:null};
		}
		else if(pos==="all")
		{
			//do nothing
		}
		else
		{
			conditions.pos=pos
		}

		LexicalEntry.find(conditions).sort({created_at:-1}).exec(
			function(err,entries){
				if(err)
				{
					return res.render('500');
				}

				res.render('lexicons/list',{
					q:q,
					entries: addCanVoteToEntries(entries,req.user),
					vote:false,
					title: "List lexicons for "+q.replace(/"/g,"'")
				});
			}
		);
	}
}

exports.create = function(req,res){
	if(req.query.label)
	{
		return res.render('lexicons/create',{
			prelabel:decodeURIComponent(req.query.label)
		});
	}
	else
	{
		return res.render('lexicons/create');
	}
}

exports.handleCreate = function(req,res){

	//TODO: check whether pos is within the predefined array

	if(req.body.label === undefined || req.body.pos === undefined || req.body.description === undefined){
		return res.render('lexicons/create', {
	        errors: utils.errors("Some required fields are not available")
	    });
	}

	var label = decodeURIComponent(req.body.label);
	var pos = req.body.pos;
	var description = decodeURIComponent(req.body.description);
	var sentence;
	if(req.body.sentence)
		sentence = decodeURIComponent(req.body.sentence);

	if(S(label).isEmpty() || S(pos).isEmpty() || S(description).isEmpty()){
		return res.render('lexicons/create', {
	        errors: utils.errors({errors:"Some required fields are empty"})
	    });
	}

	LexicalEntry.findOne({label:label,pos:pos},"label_id").sort({label_id:-1}).exec(function(err,entry){
		var label_id=1;
		if(entry != null) //incremental label_id by 1
			label_id = entry.label_id+1;

		var sentences = new Array();
		if(sentence!== undefined)
			sentences.push({text:sentence,creator:req.user.id});
		var newEntry = new LexicalEntry({
			label:label,
			label_id:label_id,
			creator:req.user.id,
			pos:pos,
			description: description,
			language:"en",
			sentences: sentences
		});

		newEntry.save(function(err,newentry){
			if(err)
			{
				console.log("err:"+err);
				return res.render('500', {
			        errors: utils.errors({errors:"there are some error."})
			    });
			}

			var user = req.user;
			user.addNewLexiconCount();
			if(sentences.length>0)
				user.addNewSentenceCredit();

			user.save(function(err,user,numberAffected){
				if(err)
					console.log("Add new lexicon:"+err);
			});
			req.flash('success',"The new Lexicon has been successfully created.");
			res.redirect('/lexicons/show?entry_id='+newentry._id.toHexString());
			return;
		});
	});	
};

exports.show=function(req,res){
	var entryId = req.query.entry_id;
	if(entryId === undefined){
		res.redirect("/lexicons/list");
		return;
	}

	LexicalEntry.findById(entryId, function(err,entry){
		if(err)
		{
			return res.render('500',{error: "Can't find the entry."});
		}

		res.render('lexicons/list',{
			entries: addCanVoteToEntries([entry],req.user),
			vote:false,
			title: "Lexicon Detail"
		});
	});
}

exports.vote=function(req,res){
	var entryIds = req.query.entryIds?req.query.entryIds.split(","):[];
	var sentence;
	if(req.query.sentence!==undefined)
		sentence = decodeURIComponent(req.query.sentence);

	if(entryIds === undefined || entryIds.length === 0)
	{
		//randomly choose a entry_id to vote
		var userId = req.user.id;
		LexicalEntry.getRandomEntry(userId,function(err,entry){
			if(err)
			{
				return res.render('500');
			}

			if(entry == null)
			{
				return res.render('500',{
					error: "There is nothing you can vote."
				});
			}

			var entries = new Array();
			entries.push(entry);

			res.render('lexicons/list',{
				entries: addCanVoteToEntries(entries,req.user),
				sentence:sentence,
				vote:true,
				title: "Vote"
			});
		});
	}
	else
	{
		var query = LexicalEntry.find();
		var qIdArray = new Array();
		for(var i=0;i<entryIds.length;i++)
		{
			var entryId = mongoose.Types.ObjectId(entryIds[i]);
			qIdArray.push({_id:entryId});
		}
		query.or(qIdArray).sort({created_at:-1}).exec(function(err,entries){
			if(err){
				return res.render('500');
			}

			if(entries === undefined || entries.length === 0)
			{
				return res.render('500',{
					error: "Can't find any lexicon."
				});
			}

			res.render('lexicons/list',{
				entries: addCanVoteToEntries(entries,req.user),
				sentence:sentence,
				vote:true,
				title: "Vote"
			});
		});
	}
}

//return json
exports.handleVoteUp=function(req,res){
	//one cannot vote you own image
	var entryId = req.body.entry_id;
	var imageId = req.body.image_id;

	if(entryId === undefined || imageId === undefined)
		return res.send('400',"Missing parameters.");

	LexicalEntry.findOne({_id:entryId,'images._id':imageId},function(err,entry){
		if(err || entry == null) 
			return res.send('500',"Can't find the lexicon.");

		var img = _.find(entry.images,function(image){return image._id.toHexString() === imageId});
		var votes = {upvote:0,downvote:0};
		
		if (img.votes.length > 0)
		{
			votes = _.countBy(img.votes,function(vote){
				return vote.val===1?'upvote':'downvote'
			});
		}

		if(votes.upvote === undefined)
			votes.upvote = 0;
		if(votes.downvote === undefined)
			votes.downvote = 0;
		
		if(img.creator === req.user.id)
			return res.send('400',"Can't vote for your own image.");

		img.votes.push({voter:req.user.id,val:1,comments:[]});

		//also find the owner of the image if possible and give him 5 points
		User.findByIdAndUpdate("5223665751e56b848c000009", {$inc:{"credits.images.credits":User.getUploadImageVoteupCredit()}},function(err,user){
				console.log("add credits for owner.");
		});

		entry.save(function(err,entry,numberAffected){
			if(err) res.send('500',"Can't save the vote.");

			var user = req.user;
			user.addUpvoteCredit();
			user.save(function(err,user,numberAffected){
				if(err)
					console.log("Upvote error:"+err);
			});

			votes.upvote+=1;
			//console.log(votes);
			return res.json(votes);
		});
	});
}

exports.handleVoteDown=function(req,res){
	//one cannot vote you own image
	var entryId = req.body.entry_id;
	var imageId = req.body.image_id;
	var reasons = req.body.reason;
	var comment = req.body.comment;

	if(entryId === undefined || imageId === undefined)
		return res.send('500',{error: "Missing parameters."});

	LexicalEntry.findOne({_id:entryId,'images._id':imageId},function(err,entry){
		if(err) 
			return res.send('500',"Can't find the lexicon.");

		var img = _.find(entry.images,function(image){return image._id.toHexString() === imageId});
		var votes = {upvote:0,downvote:0};
		
		if (img.votes.length > 0)
		{
			votes = _.countBy(img.votes,function(vote){
				return vote.val===1?'upvote':'downvote'
			});
		}

		if(votes.upvote === undefined)
			votes.upvote = 0;
		if(votes.downvote === undefined)
			votes.downvote = 0;

		//console.log("id:"+img._id);
		if(img.creator === req.user.id)
			return res.send('500',"Can't vote for your own image.");


		var newComments = _.union(reasons?reasons:[],S(comment).isEmpty()?[]:[comment]);
		img.votes.push({voter:req.user.id,val:-1,comments:newComments});
		entry.save(function(err,entry,numberAffected){
			if(err) {
				console.log(err);
				res.send('500',"Can't save the vote.");
			}
			var user = req.user;
			user.addDownvoteCredit();
			user.save(function(err,user,numberAffected){
				if(err)
					console.log("Downvote:"+err);
			});
			votes.downvote+=1;
			return res.json(votes);
		});
	});
}

exports.editLabel = function(req,res){
	var pk = req.body.pk;
	var label = req.body.value;
	if(pk === undefined){
		res.send('200',{status:'error',msg:"Bad request."});
	}

	if(S(label).isEmpty()){
		res.send('200',{status:'error',msg:"The lexicon label cannot be empty."});
	}

	//var entryId = mongoose.Types.ObjectId(pk);
	LexicalEntry.findByIdAndUpdate(pk, {label: label}, function(err,entry){
		if(err || entry == null)
			return res.send('500', 'Cannot update the lexicon');
		return res.json({label:entry.label});
	});
}

exports.editDescription = function(req,res){
	var pk = req.body.pk;
	var description = req.body.value;
	if(pk === undefined){
		res.send('200',{status:'error',msg:"Bad request."});
	}

	if(S(description).isEmpty()){
		res.send('200',{status:'error',msg:"The lexicon description cannot be empty."});
	}

	//var entryId = mongoose.Types.ObjectId(pk);
	LexicalEntry.findByIdAndUpdate(pk, {description: description}, function(err,entry){
		if(err || entry == null)
			return res.send('500', 'Cannot update the lexicon');
		console.log("description:"+entry.description);
		return res.json({description:entry.description});
	});
}

exports.addSentence = function(req,res){
	var pk = req.body.pk
	var sentence = req.body.value;

	if(pk === undefined){
		res.send('200',{status:'error',msg:"Bad request."});
	}

	if(S(sentence).isEmpty()){
		res.send('200',{status:'error',msg:"The sample sentence cannot be empty."});
	}

	//var entryId = mongoose.Types.ObjectId(pk);
	LexicalEntry.findByIdAndUpdate(pk, {$push: {"sentences": {text:sentence,creator:req.user.id}}}, function(err,entry){
		if(err || entry == null)
			return res.send('500', 'Cannot update the lexicon');

		var user = req.user;
		user.addNewSentenceCredit();
		user.save(function(err,user,numberAffected){
			if(err)
				console.log("Add sentence:"+err);
		});
		return res.json({sentence: encodeURIComponent(sentence)});
	});
}

exports.addSameAs = function(req,res){
	var pk = req.body.pk
	var sameas = req.body.value;

	console.log(pk);
	if(pk === undefined){
		res.send('200',{status:'error',msg:"Bad request."});
	}

	if(S(sameas).isEmpty()){
		res.send('200',{status:'error',msg:"The uri can't be empty."});
	}

	//var entryId = mongoose.Types.ObjectId(pk);
	LexicalEntry.findByIdAndUpdate(pk, {$push: {"sameas": {uri:sameas,creator:req.user.id}}}, function(err,entry){
		if(err || entry == null)
			return res.send('500', 'Cannot update the lexicon');

		var user = req.user;
		user.addNewSameAsCredit();
		user.save(function(err,user,numberAffected){
			if(err)
				console.log("Add sameas:"+err);
		});

		return res.json({sameas: encodeURIComponent(sameas)});
	});
}

exports.removeSentence = function(req,res){
	//implement later
}

exports.removeSameAs = function(req,res){
	//implement later
}

exports.checkDuplicate = function(req,res){
	var label = req.query.label;
	var pos = req.query.pos;
	if(label === undefined){
		return res.send('400', 'Bad Request');
	}

	if(S(label).isEmpty()){
		return res.send('400', 'Label cannot be empty.');
	}
	var conditions = {label:label};
	if(pos !== undefined){
		conditions.pos = pos;
	}
	
	LexicalEntry.find(conditions,"label description pos",function(err,entries){
		if(err)
			return res.send('500', 'Searching lexicon error');
		
		return res.json(entries);
	})
}

/**
 * get if the image has been voted by a certain user
 */
function getHasVoted(image,user)
{
	if(!user || !image)
		return 0;

	if(image.votes.length ===0)
		return 0;

	var vote = _.findWhere(image.votes,{voter:user.id});

	if(vote)
		return vote.val;
	else
		return 0;
}

/**
 * Add voted field to entries based on the user
 */
function addCanVoteToEntries(entries,user)
{
	for(var i=0;i<entries.length;i++)
	{
		var entry = entries[i];
		for(var j=0;j<entry.images.length;j++)
		{
			var up = false;
			var down = false;
			var image = entry.images[j];
			
			if(!user)
			{
				image.canvote = {up:false, down:false,val:0};
				continue;
			}
			var voted = getHasVoted(image,user); //1,-1 or false
			if(voted !== 0|| image.creator === user.id)
				image.canvote = {up:false, down:false,val:voted}; //owner or has voted
			else{
				image.canvote = {up:user.canUpvote(), down:user.canDownvote(), val:0}
			}
		}
	}

	return entries;
}
