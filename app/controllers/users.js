
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , User = mongoose.model('User')
  , LexicalEntry = mongoose.model('LexicalEntry')
  , utils = require('../../lib/utils')
  , numeral = require('numeral');

var login = function (req, res) {
  if (req.session.returnTo) {
    res.redirect(req.session.returnTo)
    delete req.session.returnTo
    return
  }
  res.redirect('/users/'+req.user.id)
}

exports.signin = function (req, res) {}

/**
 * Auth callback
 */

exports.authCallback = login

/**
 * Show login form
 */

exports.login = function (req, res) {
  res.render('users/login', {
    title: 'Login with:',
    message: req.flash('error')
  })
}

/**
 * Show sign up form
 */

exports.signup = function (req, res) {
  res.render('users/signup', {
    title: 'Sign up',
    user: new User()
  })
}

/**
 * Logout
 */

exports.logout = function (req, res) {
  req.logout()
  res.redirect('/login')
}

/**
 * Session
 */

exports.session = login

/**
 * Create user
 */

exports.create = function (req, res) {
  var user = new User(req.body)
  user.provider = 'local'
  user.save(function (err) {
    if (err) {
      return res.render('users/signup', {
        errors: utils.errors(err.errors),
        user: user,
        title: 'Sign up'
      })
    }

    // manually login the user once successfully signed up
    req.logIn(user, function(err) {
      if (err) return next(err)
      return res.redirect('/users/'+user.id+"?tour=true")
    })
  })
}

/**
 *  Show profile
 */

exports.show = function (req, res) {
  var user = req.profile
  res.render('users/show', {
      title: user.name,
      user: user,
      badge: user.getBadge(),
      totalCredits: numeral(user.getTotalCredits()).format('0,0'),
      upvotesCount: numeral(user.credits.upvotes.count).format('0,0'),
      upvotesCredits: numeral(user.credits.upvotes.credits).format('0,0'),
      downvotesCount: numeral(user.credits.downvotes.count).format('0,0'),
      downvotesCredits: numeral(user.credits.downvotes.credits).format('0,0'),
      lexiconsCount: numeral(user.credits.lexicons.count).format('0,0'),
      lexiconsCredits: numeral(user.credits.lexicons.credits).format('0,0'),
      imagesCount: numeral(user.credits.images.count).format('0,0'),
      imagesCredits: numeral(user.credits.images.credits).format('0,0'),
      sentencesCount: numeral(user.credits.sentences.count).format('0,0'),
      sentencesCredits: numeral(user.credits.sentences.credits).format('0,0'),
      sameasCount: numeral(user.credits.sameas.count).format('0,0'),
      sameasCredits: numeral(user.credits.sameas.credits).format('0,0'),
  });
  
}

/**
 * Find user by id
 */

exports.user = function (req, res, next, id) {
  User
    .findOne({ _id : id })
    .exec(function (err, user) {
      if (err) return next(err)
      if (!user) return next(new Error('Failed to load User ' + id))
      req.profile = user
      next()
    })
}
