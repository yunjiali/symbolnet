//Controller to manage the root-leve pages
var mongoose = require('mongoose');
var async = require('async');
var _=require('underscore');
var S = require('string');
var numeral = require('numeral');
var LexicalEntry = mongoose.model('LexicalEntry');
var User = mongoose.model('User');
var utils = require('../../lib/utils');
var NodeMailer = require('../mailer/nodemailer');

//NODE_ENV
var env = process.env.NODE_ENV || 'development'
var config = require('../../config/config')[env]

exports.index = function(req,res){
	async.parallel([
		function(callback){ //total lexicons
			LexicalEntry.countLexicalEntryTotal(function(err,count){
				callback(null,count);
			});
		},
		function(callback){ //total images
			LexicalEntry.countLexicalEntryWithoutImages(function(err, count){
				callback(null,count);
			});
		},
		function(callback){ //total sentences
			LexicalEntry.countSameAsTotal(function(err, count){
				callback(null,count);
			});
		},
		function(callback){
			LexicalEntry.countImagesTotal(function(err,count){
				callback(null,count);
			});
		},
		function(callback){
			LexicalEntry.countVotedImagesTotal(function(err,count){
				callback(null,count);
			});
		}
	], function(err,results){
		res.render('index',{
			totalEntry: results[0],
			totalEntryFormatted: numeral(results[0]).format("0,0"),
			totalEntryWithImage: results[0]-results[1],
			totalEntryWithImageFormatted: numeral(results[0]-results[1]).format("0,0"),
			totalSameAsFormatted: numeral(results[2]).format("0,0"),
			totalImagesFormatted:numeral(results[3]).format("0,0"),
			totalVotedImagesFormatted:numeral(results[4]).format("0,0"),
			totalVotedImagesPercentage:(results[3]===0?0:((results[4]/results[3]*100).toFixed(4)))
		});
	});
	//res.render('index');
}

exports.about=function(req,res){
    res.render('about');
}

exports.handleContactUs = function(req,res){
	//var mailer = new NodeMailer();
	var from = req.body.email
	var to = config.contactus.email
	var name = req.body.name
	var title = req.body.title
	var text = req.body.message
	var mailOptions = {
	    from: from, // sender address
	    to: to, // list of receivers
	    subject: title, // Subject line
	    text:  "From "+req.body.email+"\r\n\r\n"+text
	}

	NodeMailer.sendMail(mailOptions,function(err){
		if(err)
			res.render("500");
		req.flash('success',"Thanks for contacting us. You message has been sent to our team.");
		res.redirect('about');
	});
}