
/**
 * Formats mongoose errors into proper array
 *
 * @param {Array} errors
 * @return {Array}
 * @api public
 */

exports.errors = function (errors) {
  var keys = Object.keys(errors)
  var errs = []

  // if there is no validation error, just display a generic error
  if (!keys) {
    console.log(errors);
    return ['Oops! There was an error']
  }

  keys.forEach(function (key) {
    errs.push(errors[key].type)
  })

  return errs
}

exports.getWordNetPOS = function(tag)
{
  var wordnetPos;
  switch(tag)
  {
    case "CD": //Cardinal number          one,two
      wordnetPos="noun";
      break;
    case "JJ": // Adjective                big
      wordnetPos = "adjective";
      break;
    case "JJR": // Adj., comparative       bigger
      wordnetPos = "adjective";
      break;
    case "JJS": // Adj., superlative       biggest
      wordnetPos="adjective";
      break;
    case "LS":// List item marker         1,One
      wordnetPos="noun";
      break;
    case "NN":// Noun, sing. or mass      dog
      wordnetPos="noun";
      break;
    case "NNP":// Proper noun, sing.      Edinburgh
      wordnetPos="noun";
      break;
    case "NNPS":// Proper noun, plural    Smiths
      wordnetPos="noun";
      break;
    case "NNS":// Noun, plural            dogs
      wordnetPos="noun";
      break;
    case "RB":// Adverb                   quickly
      wordnetPos="adv";
      break;
    case "RBR":// Adverb, comparative     faster
      wordnetPos="adv";
      break;
    case "RBS":// Adverb, superlative     fastest
      wordnetPos="adv";
      break;
    case "VB":// verb, base form          eat
      wordnetPos="verb";
      break;
    case "VBD":// verb, past tense        ate
      wordnetPos="verb";
      break;
    case "VBG":// verb, gerund            eating
      wordnetPos="verb";
      break;
    case "VBN":// verb, past part         eaten
      wordnetPos="verb";
      break;
    case "VBP":// Verb, present           eat
      wordnetPos="verb";
      break;
    case "VBZ":// Verb, present           eats
      wordnetPos="verb";
      break;
    default:
      break;

  }
  return wordnetPos;
} 

exports.getStemmable = function(tag)
{
  var steammable = false;

  switch(tag)
  {
    case "JJR": // Adj., comparative       bigger
      steammable = true;
      break;
    case "JJS": // Adj., superlative       biggest
      steammable = true;
      break;
    case "NNPS":// Proper noun, plural    Smiths
      steammable = true;
      break;
    case "NNS":// Noun, plural            dogs
      steammable = true;
      break;
    case "RBR":// Adverb, comparative     faster
      steammable = true;
      break;
    case "RBS":// Adverb, superlative     fastest
      steammable = true;
      break;
    case "VBD":// verb, past tense        ate
      steammable = true;
      break;
    case "VBG":// verb, gerund            eating
      steammable = true;
      break;
    case "VBN":// verb, past part         eaten
      steammable = true;
      break;
    case "VBZ":// Verb, present           eats
      steammable = true;
      break;
    default:
      break;
  } 

  return steammable;
}

//Here is the big list:
/*
switch(tag)
  {
    case "CC": //Coord Conjuncn           and,but,or
      break;
    case "CD": //Cardinal number          one,two
      wordnetPos="noun";
      break;
    case "DT": //Determiner               the,some
      break;
    case "EX": //Existential there        there
      break;
    case "FW": //Foreign Word             mon dieu
      break;
    case "IN" //Preposition              of,in,by
      break;
    case "JJ" // Adjective                big
      wordnetPos = "adjective";
      break;
    case "JJR": // Adj., comparative       bigger
      wordnetPos = "adjective";
      break;
    case "JJS": // Adj., superlative       biggest
      wordnetPos="adjective";
      break;
    case "LS":// List item marker         1,One
      wordnetPos="noun";
      break;
    case "MD":// Modal                    can,should
      break;
    case "NN":// Noun, sing. or mass      dog
      wordnetPos="noun";
      break;
    case "NNP":// Proper noun, sing.      Edinburgh
      wordnetPos="noun";
      break;
    case "NNPS":// Proper noun, plural    Smiths
      wordnetPos="noun";
      break;
    case "NNS":// Noun, plural            dogs
      wordnetPos="noun";
      break;
    case "POS":// Possessive ending       Õs
      break;
    case "PDT":// Predeterminer           all, both
      break;
    case "PP$":// Possessive pronoun      my,oneÕs
      break;
    case "PRP":// Personal pronoun         I,you,she
      break;
    case "RB":// Adverb                   quickly
      wordnetPos="adv";
      break;
    case "RBR":// Adverb, comparative     faster
      wordnetPos="adv";
      break;
    case "RBS":// Adverb, superlative     fastest
      wordnetPos="adv";
      break;
    case "RP"://Particle                 up,off
      break;
    case "SYM":// Symbol                  +,%,&
      break;
    case "TO":// ÒtoÓ                     to
      break;
    case "UH":// Interjection             oh, oops
      break;
    case "URL":// url                     http://www.google.com/
      break;
    case "VB":// verb, base form          eat
      wordnetPos="verb";
      break;
    case "VBD":// verb, past tense        ate
      wordnetPos="verb";
      break;
    case "VBG":// verb, gerund            eating
      wordnetPos="verb";
      break;
    case "VBN":// verb, past part         eaten
      wordnetPos="verb";
      break;
    case "VBP":// Verb, present           eat
      wordnetPos="verb";
      break;
    case "VBZ":// Verb, present           eats
      wordnetPos="verb";
      break;
    case "WDT":// Wh-determiner           which,that
      break;
    case "WP":// Wh pronoun               who,what
      break;
    case "WP$":// Possessive-Wh           whose
      break;
    case "WRB":// Wh-adverb               how,where
      break;
    case ",":// Comma                     ,
      break;
    case ".":// Sent-final punct          . ! ?
      break;
    case ":":// Mid-sent punct.           : ; Ñ
      break;
    case "$":// Dollar sign               $
      break;
    case "#":// Pound sign                #
      break;
    case "\"":// quote                     "
      break;
    case "(":// Left paren                (
      break;
    case ")":// Right paren               )
      break;
    default:
      break;*/