/*!
 * Module dependencies.
 */

var async = require('async')

/**
 * Controllers
 */

var users = require('../app/controllers/users')
  , root = require('../app/controllers/root')
  , lexicons = require('../app/controllers/lexicons')
  , symbolisation = require('../app/controllers/symbolisation')
  , images = require('../app/controllers/images')
  , api = require('../app/controllers/api')
  , auth = require('./middlewares/authorization');
/**
 * Route middlewares
 */

//var articleAuth = [auth.requiresLogin, auth.article.hasAuthorization]

/**
 * Expose routes
 */

module.exports = function (app, passport) {

  // user routes
  app.get('/login', users.login)
  app.get('/signup', users.signup)
  app.get('/logout', users.logout)
  app.post('/users', users.create)
  app.post('/users/session',
    passport.authenticate('local', {
      failureRedirect: '/login',
      failureFlash: 'Invalid email or password.'
    }), users.session)
  app.get('/users/:userId', users.show)
  app.get('/auth/facebook',
    passport.authenticate('facebook', {
      scope: [ 'email', 'user_about_me'],
      failureRedirect: '/login'
    }), users.signin)
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/github',
    passport.authenticate('github', {
      failureRedirect: '/login'
    }), users.signin)
  app.get('/auth/github/callback',
    passport.authenticate('github', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/twitter',
    passport.authenticate('twitter', {
      failureRedirect: '/login'
    }), users.signin)
  app.get('/auth/twitter/callback',
    passport.authenticate('twitter', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/google',
    passport.authenticate('google', {
      failureRedirect: '/login',
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    }), users.signin)
  app.get('/auth/google/callback',
    passport.authenticate('google', {
      failureRedirect: '/login'
    }), users.authCallback)

  app.param('userId', users.user)

  // home route
  app.get('/', root.index);
  app.get('/about',root.about);
  app.post('/handleContactUs',root.handleContactUs)

  //lexicon route
  app.get('/lexicons/list', lexicons.list);
  app.post('/lexicons/list', lexicons.list);
  app.get('/lexicons/create', lexicons.create);
  app.post('/lexicons/handleCreate',auth.requiresLogin,lexicons.handleCreate);
  app.get('/lexicons/vote', auth.requiresLogin, lexicons.vote);
  app.get('/lexicons/show', auth.requiresLogin, lexicons.show);
  app.post('/lexicons/handleVoteUp', auth.requiresLoginAjax, lexicons.handleVoteUp);
  app.post('/lexicons/handleVoteDown', auth.requiresLoginAjax, lexicons.handleVoteDown);
  app.post('/lexicons/editDescription', auth.requiresLoginAjax, lexicons.editDescription);
  app.post('/lexicons/editLabel', auth.requiresLoginAjax, lexicons.editLabel);
  app.post('/lexicons/addSentence', auth.requiresLoginAjax, lexicons.addSentence);
  app.post('/lexicons/addSameAs', auth.requiresLoginAjax, lexicons.addSameAs);
  app.get('/lexicons/checkDuplicate',auth.requiresLoginAjax, lexicons.checkDuplicate);

  //images route
  app.get('/images/upload',auth.requiresLogin, images.upload);
  app.post('/images/handleUpload',auth.requiresLoginAjax, images.handleUpload);

  // symbolisation route
  app.get('/symbolisation', symbolisation.index);
  app.post('/symbolisation/show',symbolisation.show);

  //api route
  app.get('/api/symbolise', api.symbolise);

}
