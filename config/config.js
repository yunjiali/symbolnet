
var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')
  , templatePath = path.normalize(__dirname + '/../app/mailer/templates')
  , notifier = {
      service: 'postmark',
      APN: false,
      email: false, // true
      actions: ['comment'],
      tplPath: templatePath,
      key: 'POSTMARK_KEY',
      parseAppId: 'PARSE_APP_ID',
      parseApiKey: 'PARSE_MASTER_KEY'
    }

module.exports = {
  development: {
    db: 'mongodb://symbolnet:symbolnet@paulo.mongohq.com:10080/SymbolNetTest',
    //db: 'mongodb://symbolnet:symbolnet@paulo.mongohq.com:10097/SymbolNet',
    root: rootPath,
    notifier: notifier,
    app: {
      name: 'SymbolNet'
    },
    facebook: {
      clientID: "615105068555158",
      clientSecret: "d70f9e9f541b26f4289053cf91d0edba",
      callbackURL: "http://localhost:3000/auth/facebook/callback"
    },
    twitter: {
      clientID: "Ox5CX19ado28B8NRVxpbw",
      clientSecret: "V2x9UqOhABPrAHGqZtv3eH3AUgRSsSU8o9INKgQKMjI",
      callbackURL: "http://localhost:3000/auth/twitter/callback"
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/auth/github/callback'
    },
    google: {
      clientID: "930375688266.apps.googleusercontent.com",
      clientSecret: "mUfR-FQ6jgTJQBtiRReW1uBZ",
      callbackURL: "http://localhost:3000/auth/google/callback"
    },
    cloudinary:
    {
      cloud_name: "symbolnettest",
      api_key:'575244376586569',
      api_secret:"hHbo10D21CnkUr56SkxbdWoU178"
    },
    contactus:
    {
      email: "yl2@ecs.soton.ac.uk",
      pretitle: " contact you from SymbolNet"
    }
  },
  test: {
    db: 'mongodb://symbolnet:symbolnet@paulo.mongohq.com:10080/SymbolNetTest',
    root: rootPath,
    notifier: notifier,
    app: {
      name: 'Nodejs Express Mongoose Demo'
    },
    facebook: {
      clientID: "615105068555158",
      clientSecret: "d70f9e9f541b26f4289053cf91d0edba",
      callbackURL: "http://localhost:3000/auth/facebook/callback"
    },
    twitter: {
      clientID: "Ox5CX19ado28B8NRVxpbw",
      clientSecret: "V2x9UqOhABPrAHGqZtv3eH3AUgRSsSU8o9INKgQKMjI",
      callbackURL: "http://localhost:3000/auth/twitter/callback"
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/auth/github/callback'
    },
    google: {
      clientID: "APP_ID",
      clientSecret: "APP_SECRET",
      callbackURL: "http://localhost:3000/auth/google/callback"
    },
    cloudinary:
    {
      cloud_name: "symbolnettest",
      api_key:'575244376586569',
      api_secret:"hHbo10D21CnkUr56SkxbdWoU178"
    },
    contactus:
    {
      email: "yl2@ecs.soton.ac.uk",
      pretitle: " contact you from SymbolNet"
    }
  },
  production: {
    db: 'mongodb://symbolnet:symbolnet@paulo.mongohq.com:10097/SymbolNet',
    root: rootPath,
    notifier: notifier,
    app: {
      name: 'SymbolNet'
    },
    facebook: {
      clientID: "576312932425080",
      clientSecret: "7b9b5fde6dd41652cd4148e8c47f7974",
      callbackURL: "http://symbolnet.synote.org/auth/facebook/callback"
    },
    twitter: {
      clientID: "Ox5CX19ado28B8NRVxpbw",
      clientSecret: "V2x9UqOhABPrAHGqZtv3eH3AUgRSsSU8o9INKgQKMjI",
      callbackURL: "http://symbolnet.synote.org/auth/twitter/callback"
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://symbolnet.synote.org/auth/github/callback'
    },
    google: {
      clientID: "930375688266-qeggdbodcas4ual8nkhr7p48462h2731.apps.googleusercontent.com",
      clientSecret: "gnmQvpe-fPk8eWEZWPv8gURw",
      callbackURL: "http://symbolnet.synote.org/auth/google/callback"
    },
    cloudinary:
    {
      cloud_name: "symbolnet",
      api_key:'982756828326992',
      api_secret:"s8gT9BvPLtYEGAsIRt2ahngDs2I"
    },
    contactus:
    {
      email: "ead@ecs.soton.ac.uk",
      pretitle: " contact you from SymbolNet"
    }
  }
}
